//
//  ViewController.swift
//  gitlab
//
//  Created by Harsh Gajera on 29/07/22.
//

import UIKit

class ViewController: UIViewController {

    let label = UILabel()
    let saeBtn = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLabel()
        setUpBtn()
    }

    func setUpLabel() {
        label.text = "good morning"
        label.frame = CGRect(x: 11, y: 80, width: 382, height: 40)
        label.textColor = .white
        label.backgroundColor = .black
        label.textAlignment = .left
        view.addSubview(label)
    }

    func setUpBtn() {
        saeBtn.frame = CGRect(x: 11, y: 180, width: 382, height: 50)
        saeBtn.backgroundColor = .green
        saeBtn.setTitle("save", for: .normal)
//        saeBtn.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Thin", size: 30)
        saeBtn.setTitleColor(.white, for: .normal)
        saeBtn.addTarget(self, action: #selector(saveBtnAction), for: .touchUpInside)
        view.addSubview(saeBtn)
    }
    
    @objc
    func saveBtnAction() {
        label.text = "saved successful"
    }
}
